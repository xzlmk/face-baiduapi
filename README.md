### 基于springboot+百度AI的人脸考勤系统
这是一个基于springboot+百度AI的人脸考勤系统，主要有三类用户分别是**学生**、**教师**和**校长**，下面分别进行展示

#### **学生**

- 考勤记录的查看
- 个人信息的查看
- 修改登录密码

**学生登录成功后的页面**

![](https://img-blog.csdnimg.cn/20210331114326654.png)
#### **教师**
 - 菜单的查看
 - 基础设置的查看
 - 学生人脸的采集
 - 课程的开始考勤
 - 考勤记录的查看
 - 考勤图表分析的查看

**教师登录成功后的页面**

![](https://img-blog.csdnimg.cn/20210331114448828.png)

**教师端选择学生进行人脸采集**

![](https://img-blog.csdnimg.cn/20210331123220893.png)

**教师端学生进行人脸考勤**

![](https://img-blog.csdnimg.cn/20210331123355201.png)

#### **校长**
 - 菜单的查看和编辑
 - 基础设置的查看和编辑

![](https://img-blog.csdnimg.cn/20210331115048460.png)
![](https://img-blog.csdnimg.cn/2021033111510591.png)
![](https://img-blog.csdnimg.cn/20210331115138280.png)
![](https://img-blog.csdnimg.cn/2021033111515879.png)
### 源码

获取源码请加QQ群： **994793967** ，私信群主获取。群内会不定期上传项目和学习资料，群内也有考研的研友和已经上岸的学长欢迎进群交流。

![](https://img-blog.csdnimg.cn/20210331123611975.jpg)
![](https://img-blog.csdnimg.cn/20210331123611989.jpg)
![](https://img-blog.csdnimg.cn/20210331123611958.jpg)
